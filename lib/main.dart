import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
      home: Scaffold(
          backgroundColor: Colors.amber,
          appBar: AppBar(
            title: Text('Toshi Gemezh'),
            leading: Icon(Icons.menu),
            actions: <Widget>[
              Icon(Icons.favorite, color: Colors.pink),
              Icon(Icons.file_download, color: Colors.white),
            ],
            backgroundColor: Colors.black26,
          ),
          body: ListView(
            children: <Widget>[
              Container(
                  child: Image(
                image: NetworkImage(
                    'https://images.unsplash.com/photo-1575425186775-b8de9a427e67?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1868&q=80'),
              )),
              Container(
                  alignment: Alignment.center,
                  child: Text(
                    'Iam Cutie Puggie',
                    style: TextStyle(
                      fontSize: 30,
                      color: Colors.white,
                      fontWeight: FontWeight.bold),
                  )
              ),
              Container(
                alignment: Alignment.center,
                child: Text(
                  'For Sale',
                  style: TextStyle(
                      fontSize: 40,
                      color: Colors.red,
                      fontWeight: FontWeight.bold),
                ),
              ),
              Container(
               alignment: Alignment.center,
               child: Text(
                'Contact us: 081234567890',
                style: TextStyle(
                    fontSize: 15,
                    color: Colors.black,
                    fontWeight: FontWeight.bold),
              )
              )
            ],
          ))));
}
